define('AppTransport', [], function () {
   var editorExtensionId;
   var AppTransport = function (id) {
      this.editorExtensionId = id || editorExtensionId || window.editorExtensionId;
      editorExtensionId = this.editorExtensionId;
   };

   AppTransport.prototype.send = function (eventName, params, callback) {
      chrome.runtime.sendMessage(this.editorExtensionId, {
         eventName: eventName,
         params: params
      }, callback)
   };

   return AppTransport;
});