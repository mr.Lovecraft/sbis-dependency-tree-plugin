define('ContextMenu', ['ContextMenuHandlers'], function (ContextMenuHandlers) {
   var $body = $('body'),
      isVisible;

   $body.on("contextmenu", false);

   $('<div class="copied-module-field"><textarea></textarea></div>').appendTo($body);

   var ContextMenu = function (target) {
     this.targetModuleName = target.children[0].innerHTML;
      this.targetModuleNode = target.parentNode;
   };

   ContextMenu.prototype.show = function (position) {
      $('<div class="context-menu"></div>')
         .css(position)
         .appendTo($body)
         .append($('<ul/>')
            .append('<li class="context-menu-point copy">Скопировать имя</li>')
            .append('<li class="context-menu-point remove">Удалить модуль</li>')
            .append('<li class="context-menu-point add">Добавить модуль</li>')
            .append('<li class="context-menu-point disabled parents">Вершины, тянущие этот модуль</li>')
         )
         .show('fast');

      $('.context-menu-point.copy').bind('click', ContextMenuHandlers.copy.bind(null, this.targetModuleName));
      $('.context-menu-point.remove').bind('click', ContextMenuHandlers.remove.bind(null, this.targetModuleName, this.targetModuleNode));
      $('.context-menu-point.add').bind('click', ContextMenuHandlers.add.bind(null, this.targetModuleName, this.targetModuleNode));
       $('.context-menu-point.parents').bind('click', ContextMenuHandlers.parents.bind(null, this.targetModuleName));
   };

   function showMenu(e) {
      var contextMenu = new ContextMenu(e.target);
      contextMenu.show({
         left: e.pageX+'px',
         top: e.pageY+'px'
      });
   }
   return function () {
      var $circles = $('circle');
      $circles.bind('mousedown', function (e) {
         if (e.which == 3) {
            if (!isVisible) {
               showMenu(e);
               isVisible = true;
            } else {
               $('.context-menu').remove();
               showMenu(e);
            }
         }
      });

      $body.bind('click', function (e) {
         if (isVisible) {
            $('.context-menu').remove();
            isVisible = false;
         }
      })
   };
});

define('ContextMenuHandlers', ['AppTransport', 'discoverModulesChain', 'ParentFinder'], function (AppTransport, discoverModulesChain, ParentFinder) {
   var appTransport = new AppTransport();
   return {
      copy: function (target, e) {
         var copyField = $('.copied-module-field textarea');
         copyField.val(target);
         copyField.select();
         document.execCommand('copy');
      },
      remove: function (targetModuleName, targetModuleNode) {
        var recursiveStructModule = discoverModulesChain(targetModuleNode.id, treeData[0]);
         appTransport.send('removeModule', {
            parentModule: recursiveStructModule.parent ? recursiveStructModule.parent.name : '',
            moduleToDelete: targetModuleName
         }, function () {
            location.reload();
         });
      },
      add: function (targetModuleName) {
         appTransport.send('addModule', {
            parentModule: targetModuleName,
            moduleToAdd: prompt('Введите имя модуля, который хотите добавить в зависимости')
         }, function () {
            location.reload();
         });
      },
      parents: function (target, e) {
         var parents = ParentFinder(target, {});
         if (!parents.length) {
            parents = ParentFinder('js!' + target, {});
         }

         chrome.tabs.getCurrent(function(props) {
            appTransport.send('sendModuleParents', {
               currentTab: props.id,
               parents: parents,
               child: target
            }, function () {});
         });
         console.log('parents');
      }
   }
});