define('ColorLink', function () {
   function colorLink(tail) {
      for (var i=0; i < tail.length - 1; i++) {
         var direction = tail[i + 1] + '->' + tail[i],
            $path = $('.link[direction="' + direction + '"]');
         $path.addClass('coloredLink');
      }
   }

   return colorLink;
});

/**
 * Ищет первое вхлождение модуля с переданным id и выделяет его большим фиолетовым кругом.
 * Ищет его копии и выделяет их малыми кругами.
 * Возвращает true, если это единственный узел
 */
define('SearchOrigin', function () {
   function searchOrigin(id) {
      var curElem = $("#" + id),
         originId = curElem.attr("origin"), copies;
      if(!curElem.length) return;

      if (!originId) {
         copies = curElem.attr("copies").split("&");
         if(copies.length == 1 && copies[0] == '') return true;
         showAllCopies(copies);
         return;
      }

      var originElem = $("#" + originId);
      copies = originElem.attr("copies").split("&");
      curElem.find("circle").addClass("duplicateNodes");
      originElem.find("circle").addClass("duplicateNodes").addClass("origin");
      showAllCopies(copies)
   }

   return searchOrigin;
});

/**
 * Скрывает все названия модулей и их подсветку, исключая корень
 */
define('ClearModules', ['ShowTitles'], function (showTitles) {
   function clearModules(allTexts) {
      allTexts = $("g").find('text');
      allTexts.addClass("hiddenText");
      $('.markedCircle').removeClass('markedCircle');
      $('.duplicateNodes').removeClass('duplicateNodes');
      $('.coloredLink').removeClass('coloredLink');
      showTitles([1])
   }

   return clearModules;
});

/**
 * Показывавет названия модулей, присутствующие в переданном массиве-цепочке
 */
define('ShowTitles', function () {
   function showTitles(tail) {
      tail.forEach(function (elem) {
         var $elem = $("#" + elem), $text = $elem.find("text"), $circle = $elem.find("circle");
         $text.removeClass("hiddenText");
         $circle.addClass("markedCircle")
      })
   }

   return showTitles;
});

function showAllCopies(copiesId) {
   copiesId.forEach(function (id) {
      $("#" + id).find("circle").addClass("duplicateNodes")
   })
}