/**
 * Формирует путь от переданного модуля до корня. Возвращает массив-цепочку.
 */
define('GetParentTail', function () {
   function getParentTail(module) {
      var tail = [];

      while (module && module != "null") {
         if (module && module.id) {
            tail.push(module.id);
            module = module.parent
         }
      }
      return tail
   }

   return getParentTail;
});

/**
 * Возвращает модуль с указанным id
 */
define('discoverModulesChain', function () {
   function discoverModulesChain(id, module) {
      if (module.id == id) {
         return module
      }
      if (module.children) {
         for (var i = 0; i < module.children.length; i++) {
            var mod = discoverModulesChain(id, module.children[i]);
            if (mod) {
               return mod
            }
         }
      }
   }

   return discoverModulesChain;
});

/**
 * Записывает в массив modules найденные модули с указанными именами
 */
define('DiscoverModulesChainByName', function () {
   function discoverModulesChainByName(name, module, modules) {

      if (module.name == name) {
         modules.push(module);
      }
      if (module.children) {
         for (var i = 0; i < module.children.length; i++) {
            discoverModulesChainByName(name, module.children[i], modules);

         }
      }
   }

   return discoverModulesChainByName;
});

define('FindModulePaths', ['ColorLink', 'SearchOrigin', 'GetParentTail', 'ClearModules', 'DiscoverModulesChainByName'], function (colorLink, searchOrigin, getParentTail, clearModules, discoverModulesChainByName) {
   function findModulePaths(name) {
      $('#findField').val(name);

      clearModules();
      if(!name) return;

      var modules = [],
         tails = [];

      discoverModulesChainByName(name, treeData[0], modules);

      modules.forEach(function (mod) {
         tails.push(getParentTail(mod));
      });

      tails.forEach(function (tail) {
         colorLink(tail);
         searchOrigin(tail[0]);
      });

      if (tails.length == 1) {
         $('#' + tails[0][0] + ' circle').addClass('duplicateNodes');
      }
   }

   return findModulePaths;
});

define('CheckExist', function () {
   function checkExist(name, errback) {
      if (name in links) {
         return name;
      }

      if (!(name in links) && "js!" + name in links) {
         return 'js!' + name
      }

      if(errback) errback();
      return false;
   }

   return checkExist;
});

define('ParentFinder', function () {

   var ParentFinder = function (cfg) {
      this._modulesList = {};
      this._searchingRegExp = new RegExp(cfg.regExp);
      this._result = {};
   };

   // ParentFinder.prototype.search = function (name, tail) {
   //    for (var p in this._modulesList) {
   //       var tailArray = tail || [];
   //       if (!this._modulesList.hasOwnProperty(p) || checkTail(tailArray, p) || !this._searchingRegExp.test(p)) continue;
   //       if (this._modulesList[p].indexOf(name) > -1) {
   //          tailArray.push({name: p});
   //          this.search(p, tailArray);
   //          var last = tailArray.pop();
   //          if (last && !last.tailed) {
   //             tailArray.forEach(function (tailElem) {
   //                tailElem.tailed = true
   //             });
   //
   //             this._result[last.name] = true;
   //          }
   //       }
   //    }
   // };

   ParentFinder.prototype.search = function (name) {
      for (var p in this._modulesList) {
         if (!this._modulesList.hasOwnProperty(p)) continue;
         if (this._modulesList[p].indexOf(name) > -1) {
            this._result[p] = true;
         }
      }
   };

   function checkTail(tailArray, name) {
      var tailed;
      tailArray.forEach(function (tailElem) {
         if (tailElem.name == name) {
            tailed = true;
         }
      });

      return tailed;
   }

   ParentFinder.prototype.getModuleList = function () {
      for (var p in links) {
         if (!links.hasOwnProperty(p)) continue;
         if (Object.keys(links[p]).length) {
            this._modulesList[p] = links[p]
         }
      }
   };

   ParentFinder.prototype.getResult = function () {
      return Object.keys(this._result);
   };

   return function (name, options) {
      var parentFinder = new ParentFinder(options);
      parentFinder.getModuleList();
      parentFinder.search(name);
      return parentFinder.getResult();
   }
});