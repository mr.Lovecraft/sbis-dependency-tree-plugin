define('dragstart', function () {
   var
      $body,
      $doc,
      ratio,
      mouseposX,
      mouseposY,
      to;

   function drag(e) {
      clearTimeout(to);
      var deltaX = (mouseposX - e.screenX) * ratio,
         deltaY = (mouseposY - e.screenY) * ratio;
      to = setTimeout(function () { // таймаут чтобы события от мыши не перекрывали друг друга,
         $doc.scrollLeft($doc.scrollLeft() + deltaX);
         $doc.scrollTop($doc.scrollTop() + deltaY);
         mouseposX = e.screenX;
         mouseposY = e.screenY;
      }, 1);
   }

   function dragstop() {
      $body.css('cursor', '');
      $doc.unbind('mousemove.drag'); //отключаем свой mousemove.
   }

   function dragstart(body, doc, e) {
      if(e.target.className.indexOf && e.target.className.indexOf('noblock') > -1) {
         return;
      }

      console.log('> dragstart');
      if (!$body) {
         $body = body;
      }

      if (!$doc) {
         $doc = doc;
      }

      if (!ratio) {
         ratio = ratio || $doc.width() / $(window).width();
      }
      e.preventDefault();
      $body.css('cursor', 'pointer');
      mouseposX = e.screenX;
      mouseposY = e.screenY;
      $doc.on('mousemove.drag', drag);
      $doc.one('mouseup', dragstop);
   }

   return dragstart;
});

define('clickAll', [
   'discoverModulesChain',
   'GetParentTail',
   'ClearModules',
   'ShowTitles',
   'SearchOrigin'
], function (discoverModulesChain, getParentTail, clearModules, showTitles, searchOrigin) {
   function clickAll(e) {
      var mod = discoverModulesChain(this.id, treeData[0]),
         tail = getParentTail(mod);

      if (e.altKey) {
         var
            $this = $(this),
            thisNode = discoverModulesChain(tail[0], treeData[0]),
            parentNode = discoverModulesChain(tail[1], treeData[0]),
            circle = $this.find('circle');

         circle.addClass('nodeToDelete');
         chrome.runtime.sendMessage(editorExtensionId, {
            type: 'deleteModule',
            body: {
               parentModule: parentNode.name,
               moduleToDelete: thisNode.name
            }
         }, function () {});
          e.stopImmediatePropagation()
      } else {
         clearModules();
         showTitles(tail);
         searchOrigin(this.id);
         e.stopImmediatePropagation()
      }
   }

   return clickAll;
});

define('clickHelper', function () {
   function clickHelper() {
      $(this).css("display", "none")
   }

   return clickHelper;
});

define('moduleChange', ['AppTransport'], function (AppTransport) {
   var appTransport = new AppTransport();
   function moduleChange() {
      appTransport.send('refreshModule', {
         module: $(this).val(),
         findModule: ''
      }, function () {
         location.reload();
      });
   }

   return moduleChange;
});

define('findChange', ['FindModulePaths'], function (findModulePaths) {
   function findChange($findFieldText) {
      $findFieldText.val(this.val());
      findModulePaths(this.val());
   }

   return findChange;
});