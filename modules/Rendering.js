define('Main', [
   'TakeChain',
   'Render',
   'dragstart',
   'clickAll',
   'clickHelper',
   'moduleChange',
   'findChange',
   'ClearModules',
   'FindModulePaths',
   'ContextMenu'
], function (takeChain, render, dragstart, clickAll, clickHelper, moduleChange, findChange, clearModules, findModulePaths, ContextMenu) {
   function main(entryPoint, findModule) {
      takeChain(entryPoint, treeData);
      render();

      var $allG = $("g"),
         $svg = $("svg"),
         svgWidth = parseInt($svg.attr("width")),
         svgHeight = parseInt($svg.attr("height")),
         $body = $("body"),
         $helper = $("#helper"),
         $helperMainText = $helper.find("#mainText")[0],
         $moduleField = $('#moduleField'),
         $moduleFieldText = $('#moduleFieldText'),
         $findField = $('#findField'),
         $findFieldText = $('#findFieldText'),
         $controls = $('.isControl'),
         allowedModules = Object.keys(uniqueNames).filter(function (name) {
            if (name in links) return true;
         }).sort(),

         $doc = $(document),
         zoomed = true;


      currentModule = entryPoint;
      $("h2").css("display", "none");
      $moduleFieldText.val(entryPoint);
      $findField.val(findModule);
      $findFieldText.val(findModule);
      $helper.css("display", "");
      $controls.find('circle').addClass('markedControl');

      d3.select('#moduleField').selectAll('option').data(Object.keys(links).filter(function (e) {
         if (blacklist.indexOf(e) == -1) return true;
      }).sort()).enter().append('option').text(function (d) {
         return d;
      }).attr('selected', function (d) {
         if (d == entryPoint) return true
      });


      allowedModules.unshift('---');
      d3.select('#findField').selectAll('option').data(allowedModules).enter().append('option').text(function (d) {
         return d;
      });
      var clientWidth = window.screen.availWidth,
         clientHeight = window.screen.availHeight,
         zoomX = clientWidth / svgWidth,
         zoomY = clientHeight / svgHeight, zoomInd;

      $svg.attr("width", svgWidth + 400);

      if (zoomX < 0.5 || zoomY < 0.5) {
         zoomed = false;
         zoomInd = zoomX < 0.5 ? 0.5 : zoomY < 0.5 ? 0.5 : zoomY;

         $helperMainText.innerHTML +="Для увеличения, зажмите <span class='blueText'>Ctrl</span> и <span class='blueText'>кликните</span> на интересующую область экрана. " +
            "Чтобы вернуться к прежнему виду, нажмите <span class='blueText'>Esc.</span>";

         document.addEventListener("click", function (e) {
            if (zoomed)return;
            if (!e.ctrlKey)return;
            zoomed = true;
            var x = window.scrollX - (clientWidth / 2 - e.clientX), y = window.scrollY - (clientHeight / 2 - e.clientY);
            $svg.animate({zoom: 1}, 500);
            $body.animate({scrollLeft: x * 2 + clientWidth / 2}, 1500).animate({scrollTop: y * 2 + clientHeight / 2}, 1500)
         });

         document.addEventListener("keydown", function (e) {
            if (e.keyCode == 27 && zoomed) {
               zoomed = false;
               var origX = window.scrollX, origY = window.scrollY;
               $svg.css({zoom: zoomInd || 1});
               scrollTo(origX / 2 - window.screen.availWidth / 4, origY / 2 - window.screen.Height / 4)
            }
         });
      }

      //<Биндинги>

      $doc.bind('mousedown', '#content', dragstart.bind(null, $body, $doc));

      $svg.css({zoom: zoomInd || 1});
      clearModules();
      if (findModule) findModulePaths(findModule);

      $allG.bind("click", clickAll);

      $helper.bind("click", clickHelper);

      $moduleField.bind('change', moduleChange);

      $findField.bind('change', findChange.bind($findField, $findFieldText));

      ContextMenu();

      //</Биндинги>
   }

   return main;
});

define('TakeChain', function () {
   function takeChain(entry, nodeData, parent, tail) {
      if (blacklist.indexOf(entry) > -1) return;

      count++;
      if (!tail) {
         tail = []
      }

      var deps = links[entry] || [], myNode, cycledMember;
      nodeData.push(
         {
            name: entry,
            parent: parent || "null",
            id: ++id
         });
      myNode = nodeData[nodeData.length - 1];

      if (deps.length) {
         myNode.children = []
      }

      if (tail.indexOf(entry) > -1) {
         myNode.isCycled = true;
         return entry
      }
      if (!(entry in uniqueNames)) {
         uniqueNames[entry] = {usages: 1, id: myNode.id, copies: ""}
      } else {
         uniqueNames[entry].usages++;
         uniqueNames[entry].copies += myNode.id + "&";
         myNode.origId = uniqueNames[entry].id;
         return
      }

      tail.push(entry);
      maxDepth = Math.max(maxDepth, tail.length);

      deps.forEach(function (d) {
         var getCycledMember = takeChain(d, myNode.children, myNode.name, tail);
         if (getCycledMember) {
            myNode.isCycled = true
         }
         if (myNode.name == getCycledMember) {
            cycledMember = getCycledMember = null
         }
         cycledMember = !cycledMember ? getCycledMember : cycledMember
      });

      tail.pop();
      return cycledMember
   }

   return takeChain;
});

define('Render', function () {
   function render() {
      var margin = {
            top: 40,
            right: 120,
            bottom: 20,
            left: 50
         },
         radius = 7,
         eps = 100,
         normalizeY = 100,
         width = count * radius + eps,
         height = maxDepth * normalizeY + eps,
         root = treeData[0];

      var tree = d3.layout.tree().size([width, height]);
      var diagonal = d3.svg.diagonal().projection(function (d) {
         return [d.x, d.y]
      });

      var svg = d3.select("body")
         .append("svg")
         .attr("width", width)
         .attr("height", height)
         .append("g")
         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      update(root);

      function update(source) {
         var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

         nodes.forEach(function (d) {
            d.y = d.depth * normalizeY
         });

         var node = svg.selectAll("g.node").data(nodes);

         var nodeEnter = node.enter()
            .append("g")
            .attr("class", function (d) {
               return d.nameSpace == 'SBIS3.CONTROLS' ? 'node isControl' : 'node';
            })
            .attr("transform", function (d) {
               return "translate(" + d.x + "," + d.y + ")"
            })
            .attr("id", function (d) {
               return d.id
            })
            .attr("origin", function (d) {
               return d.origId ? d.origId : ""
            })
            .attr("copies", function (d) {
               if (uniqueNames[d.name].id == d.id && uniqueNames[d.name].copies) {
                  return uniqueNames[d.name].copies
               } else {
                  return ""
               }
            });

         nodeEnter.append("text").attr("x", function () {
            return 10
         })
            .attr("y", function () {
               return -10
            })
            .text(function (d) {
               return d.name
            });

         nodeEnter.append("circle")
            .attr("r", radius)
            .style("fill", function (d) {
               return d.isCycled ? CYCLED_COLOR : DEFAULT_COLOR;
            })
            .style("cursor", "pointer")
            .append("svg:title")
            .text(function (d) {
               return d.name
            });

         var link = svg.selectAll("path.link").data(links, function (d) {
            return d.target.id
         });

         link.enter().insert("path", "g")
            .attr("class", function (d) {
               var isCycledLink = d.source.isCycled && d.target.isCycled;
               return isCycledLink ? "link cycledLink" : "link"
            })
            .attr("d", diagonal)
            .attr("direction", function (d) {
               return d.source.id + '->' + d.target.id;
            })
      }
   }

   return render;
});