define('InfoPanel', ['AppTransport'], function (AppTransport) {
   var $body = $('body');
   var InfoPanel = function () {
      this._isVisible = false;

   };

   InfoPanel.prototype.show = function () {
      $('<div id="InfoPanel"></div>').appendTo($body);
   };
});