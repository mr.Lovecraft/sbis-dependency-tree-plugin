var links,
   module,
   findModule,
   renderingID;

var TAB_STORAGE = {};

var Receiver = function () {
   this._events = {};
};

Receiver.prototype.notify = function (eventName, params, sendResponse) {
   this._events[eventName](params, sendResponse);
};

Receiver.prototype.on = function (eventName, handler) {
   this._events[eventName] = handler
};

var receiver = new Receiver();
receiver.on('refreshModule', function (params, sendResponse) {
   module = params.module;
   findModule = params.findModule;
   sendResponse('refreshed');
});

receiver.on('getModulesInfo', function (params, sendResponse) {
   sendResponse({
      links: links,
      module: module,
      findModule: findModule
   });
});

receiver.on('removeModule', function (params, sendResponse) {
   var
      parentModule = links[params.parentModule],
      moduleToDelete = params.moduleToDelete,
      indexOfModuleToDelete = parentModule.indexOf(moduleToDelete);
   if (indexOfModuleToDelete > -1) {
      parentModule.splice(indexOfModuleToDelete, 1);
   }

   sendResponse();
});

receiver.on('addModule', function (params, sendResponse) {
   var
      parentModule = links[params.parentModule],
      moduleToAdd = params.moduleToAdd;

   if (!links[moduleToAdd]) {
      links[moduleToAdd] = [];
   }

   if (parentModule.indexOf(moduleToAdd) == -1) {
      parentModule.push(moduleToAdd);
   }

   sendResponse();
});

receiver.on('sendModuleParents', function (params, sendResponse) {
   sendResponse();
   chrome.tabs.create({url: 'parents.html#' + params.currentTab}, function (tab) {
      TAB_STORAGE[params.currentTab] = {
         parentsPage: tab.id,
         parents: params.parents,
         child: params.child
      };
   });
});

receiver.on('getModuleParents', function (params, sendResponse) {
   sendResponse(TAB_STORAGE[params.id]);
});


receiver.on('refreshModuleTree', function (params, sendResponse) {
   module = params.module;
   chrome.tabs.reload(params.id, {}, function () {

   });
});

chrome.extension.onConnect.addListener(function (port) {

   port.onMessage.addListener(function (message) {
      module = message.module;
      links = message.links;

      chrome.tabs.create({url: 'rendering.html'}, function (tab) {
         TAB_STORAGE[tab.id] = {};
      });
   });
});

chrome.runtime.onMessage.addListener(
   function (request, sender, sendResponse) {
      receiver.notify(request.eventName, request.params, sendResponse);
      return true;
   });