chrome.runtime.onMessage.addListener(function (msg, sender, response) {

   if ((msg.from === 'popup')) {
      var elt = document.createElement("script"),
         resourcePath;
      elt.innerHTML = "sessionStorage.setItem('SDT-Service', window.wsConfig ? window.wsConfig.resourceRoot : '');";
      document.head.appendChild(elt);

      resourcePath = sessionStorage.getItem('SDT-Service');
      delete sessionStorage['SDT-Service'];

      response(resourcePath);
   }
});