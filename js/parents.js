var editorExtensionId = location.href,
   hash = parseInt(location.hash.replace('#', ''), 10);
editorExtensionId = editorExtensionId.match(/.*:\/\/(.*)?\/./)[1];

$(document).ready(function () {

   requirejs(['AppTransport'], function (AppTransport) {
      var appTransport = new AppTransport(editorExtensionId);

      function showParentList(parentList) {
         var parentListTable = $('#parentListTable');
         parentList.forEach(function (pl) {
            addRow(parentListTable, [pl]);
         });
      }

      function addRow(table, data) {
         table.append('<tr></tr>');

         var tr = table.find('tr:last');

         data.forEach(function (d) {
            tr.append('<td>' + d + '</td>')
         });

         tr.bind('click', function () {
            var $this = $(this);
            appTransport.send('refreshModuleTree', {
               module: $this.find('td').toArray().pop().innerHTML,
               id: hash

            }, function () {

            });
         });
      }

      appTransport.send('getModuleParents', {id: hash}, function (response) {
         var parentList = response.parents,
            childName = response.child;
         $('h2').text('Родители модуля "' + childName + '"');
         showParentList(parentList);
      });
   });
});