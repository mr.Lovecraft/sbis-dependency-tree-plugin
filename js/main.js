var treeData = [],
   links,
   blacklist = ["bootup", "browser", "cdn", "css", "datasource", "eve", "fs", "help", "html", "i18n", "is", "is-api", "jquery", "js", "text", "json", "optional", "preload"],
   uniqueNames = {},
   count = 0,
   maxDepth = 0,
   id = 0,
   CYCLED_COLOR = "red",
   DEFAULT_COLOR = "#fff",
   editorExtensionId = location.href,
   currentModule;
   editorExtensionId = editorExtensionId.replace("chrome-extension://", "").replace("/rendering.html", "");

$(document).ready(function () {
   requirejs(['Main', 'CheckExist', 'FindModulePaths', 'AppTransport', 'ParentFinder'], function (main, checkExist, findModulePaths, AppTransport, ParentFinder) {
      var appTransport = new AppTransport(editorExtensionId),
         findParentInput = $('#moduleParentText'),
         moduleParentCheckBoxInput = $('#moduleParentCheckBox');

      findParentInput.bind('click', function () {
         moduleParentCheckBoxInput.prop('checked', true);
      });

      document.addEventListener("keydown", function (e) {
         if(e.keyCode == 13) {
            e.preventDefault();
            var module = $('#moduleFieldText').val(),
               findModule = $('#findFieldText').val(),
               findParent = findParentInput.val(),
               moduleParentCheckBox = moduleParentCheckBoxInput.prop('checked', true);

            if (moduleParentCheckBox && findParent) {

               var parents = ParentFinder(findParent, {});
               if (!parents.length) {
                  parents = ParentFinder('js!' + findParent, {});
               }


               chrome.tabs.getCurrent(function(props) {
                  appTransport.send('sendModuleParents', {
                     currentTab: props.id,
                     parents: parents,
                     child: findParent
                  }, function () {});
               });
               return;
            }

            if(module == currentModule) {
               findModule = checkExist(findModule);

               findModulePaths(findModule);
               return;
            }

            appTransport.send('refreshModule', {
               module: module,
               findModule: ''
            }, function () {
               location.reload();
            });
         }
      });

      appTransport.send('getModulesInfo', {}, function (response) {
         var entryPoint = response.module,
            findModule = response.findModule;
         links = response.links;

         entryPoint = checkExist(entryPoint, function() {
            $("h2").text('Модуль ' + entryPoint + ' не найден.');
         });

         if (findModule) {
            findModule = checkExist(findModule, function() {
               $("h2").text('Модуль ' + findModule + ' не найден.');
            });
         }

         if(!entryPoint || findModule === false) return;

         main(entryPoint, findModule)
      })
   })
});