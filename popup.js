$(document).ready(function() {

   var port = chrome.extension.connect({name: "popup communication"});
   var $moduleDepsInputCheckBox = $('#moduleDepsInputCheckBox');
   var $moduleDepsInput = $('#moduleDepsInput');
   var $moduleField = $('#moduleField');
   var $span = $('span');
   var links;
   var customLinks;
   var ORIGINAL_MDEPS_NOT_ALLOWED;
   var USE_CUSTOM_MDEPS;
   var LOADING_BLOCK = true;
   var BAD_JSON;

   $moduleField.bind('keypress', function (e) {
      if(e.keyCode == 13) {
         var module = $moduleField.val();

         if(!module) {
            return;
         }

         console.log(LOADING_BLOCK, USE_CUSTOM_MDEPS, customLinks);
         if (!LOADING_BLOCK) {
            var linksToLoad = USE_CUSTOM_MDEPS ? customLinks : links;
            if (linksToLoad) {
               port.postMessage({
                  module: module,
                  links: linksToLoad
               })
            }
         }
      }
   });

   $moduleDepsInput.bind("change", function () {
      LOADING_BLOCK = true;
      customLinks = null;
      var fileList = this.files; /* now you can work with the file list */
      var reader = new FileReader();
      reader.onload = function(e) {
         var mDeps = reader.result;
         try {
            mDeps = JSON.parse(mDeps);
            customLinks = mDeps.links;
            LOADING_BLOCK = false;
         } catch (e) {
            LOADING_BLOCK = false;
            console.error('Загруженный module-dependencies.json не валиден', e);
         }
      };
      reader.readAsText(fileList[0]);
   });
   $moduleDepsInputCheckBox.bind('change', function () {
      USE_CUSTOM_MDEPS = $moduleDepsInputCheckBox.prop('checked');

      if (ORIGINAL_MDEPS_NOT_ALLOWED) {
         if (USE_CUSTOM_MDEPS) {
            showModuleInputs();
         } else {
            hideModuleInputs();
         }
      }
   });

    chrome.tabs.query(
       {
           currentWindow: true,
           active: true
       },
       function (foundTabs) {
           if (!foundTabs.length) return;

           chrome.tabs.sendMessage(foundTabs[0].id,
              {from: 'popup'}, function (resourcePath) {
                  $span.text('Не удалось загрузить module-dependencies.json');

                  var currentLocation = getLocation(foundTabs[0].url);
                  currentLocation = currentLocation.protocol + '//' + currentLocation.host + (resourcePath || '/resources/');
                  checkModuleDeps(currentLocation);
              }
           );
       }
    );

   function showModuleInputs() {
      $span.attr('hidden', true);
      $moduleField.removeAttr('hidden');
   }

   function hideModuleInputs() {
      $span.removeAttr('hidden');
      $moduleField.attr('hidden', true);
   }

   function checkModuleDeps(currentLocation) {
      $.ajax({
         type: "GET",
         async: true,
         url: currentLocation + 'module-dependencies.json',
         success: function(message){
            links = message.links;
            showModuleInputs();
            LOADING_BLOCK = false;
         },
         error: function () {
            ORIGINAL_MDEPS_NOT_ALLOWED = true;
         }

      })
   }

   var getLocation = function(href) {
      var l = document.createElement("a");
      l.href = href;
      return l;
   };
});